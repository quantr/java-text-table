
import hk.quantr.javatexttable.TextTable;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MainTest {

	@Test
	public void mainTest() {
		MyTableModel model = new MyTableModel();
		model.data.add(new String[]{"Peter", "39", "Hong Kong", "789.432"});
		model.data.add(new String[]{"Joanna", "19", "China", "12.432"});
		model.data.add(new String[]{"Vincent", "29", "Hong Kong", "123213.432"});
		model.data.add(new String[]{"Kenneth", "39", "Japan", "234234.432"});
		model.data.add(new String[]{"Martin", "49", "Korean", "4563.432"});
		MyTableRenderer renderer = new MyTableRenderer();
		TextTable table = new TextTable(model, renderer);
		table.eraseScreen = true;
		table.showGridLine = true;
		table.margin = 2;
		table.sortColumn = 0;
		table.ascending = false;
		System.out.println(table.render());
	}
}
