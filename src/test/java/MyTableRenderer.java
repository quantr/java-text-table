
import hk.quantr.javatexttable.TableRenderer;
import javax.swing.JTable;
import org.fusesource.jansi.Ansi.Color;
import static org.fusesource.jansi.Ansi.Color.*;
import static org.fusesource.jansi.Ansi.ansi;

/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyTableRenderer implements TableRenderer {

	@Override
	public String render(JTable table, Object value, int row, int col) {
		// color refer to https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
		String str = (String) value;
		if (col == 0) {
			Color c = RED;
			c.fgBright();
			return ansi().fg(WHITE).bg(c).a(str).reset().toString();
		} else if (col == 1) {
			Color c = BLUE;
			c.fgBright();
			return ansi().fg(WHITE).bg(c).a(str).reset().toString();
		} else {
			return ansi().fg(GREEN).a(str).reset().toString();
		}
	}

}
