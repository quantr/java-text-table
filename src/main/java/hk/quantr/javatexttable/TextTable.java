/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package hk.quantr.javatexttable;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.apache.commons.lang.StringUtils;
import static org.fusesource.jansi.Ansi.ansi;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TextTable {

	TableModel model;
	TableRenderer renderer;
	public boolean eraseScreen;
	public boolean showGridLine;
	public int margin;
	public int sortColumn = -1;
	public boolean ascending = true;

	public TextTable(TableModel model) {
		this.model = model;
	}

	public TextTable(TableModel model, TableRenderer renderer) {
		this.model = model;
		this.renderer = renderer;
	}

	public String render() {
		JTable table = new JTable(model);
		if (sortColumn != -1 && sortColumn >= 0 && sortColumn < model.getColumnCount()) {
			TableRowSorter sorter = new TableRowSorter(model);
			List<RowSorter.SortKey> sortKeys = new ArrayList<>();
			sortKeys.add(new RowSorter.SortKey(sortColumn, ascending ? SortOrder.ASCENDING : SortOrder.DESCENDING));
			sorter.setSortKeys(sortKeys);
			table.setRowSorter(sorter);

//			for (int row = 0; row < table.getRowCount(); row++) {
//				for (int col = 0; col < table.getColumnCount(); col++) {
//					System.out.print(table.getValueAt(row, col) + "\t");
//				}
//				System.out.println();
//			}
		}

		StringBuilder str = new StringBuilder();
		if (eraseScreen) {
			ansi().eraseScreen().reset();
		}

		int[] numberOfCharOfEachCol = numberOfCharOfEachCol(table);

		if (showGridLine) {
			for (int col = 0; col < table.getColumnCount(); col++) {
				str.append("+" + StringUtils.repeat("-", margin));
				str.append(StringUtils.repeat("-", numberOfCharOfEachCol[col]));
				str.append(StringUtils.repeat("-", margin));
			}
			str.append(System.lineSeparator());
		}

		for (int col = 0; col < table.getColumnCount(); col++) {
			if (showGridLine) {
				str.append("|");
			}
			str.append(StringUtils.repeat(" ", margin) + table.getColumnName(col) + StringUtils.repeat(" ", numberOfCharOfEachCol[col] - table.getColumnName(col).length()) + StringUtils.repeat(" ", margin));
		}
		str.append(System.lineSeparator());

		if (showGridLine) {
			for (int col = 0; col < table.getColumnCount(); col++) {
				str.append("+" + StringUtils.repeat("-", margin));
				str.append(StringUtils.repeat("-", numberOfCharOfEachCol[col]));
				str.append(StringUtils.repeat("-", margin));
			}
			str.append(System.lineSeparator());
		}

		for (int row = 0; row < table.getRowCount(); row++) {
			for (int col = 0; col < table.getColumnCount(); col++) {
				if (showGridLine) {
					str.append("|");
				}
				str.append(StringUtils.repeat(" ", margin));
				String renderStr = null;
				if (renderer == null) {
					renderStr = table.getValueAt(row, col).toString();
				} else {
					renderStr = renderer.render(table, table.getValueAt(row, col), row, col);
				}
				str.append(renderStr);
				str.append(StringUtils.repeat(" ", numberOfCharOfEachCol[col] - table.getValueAt(row, col).toString().length()));
				str.append(StringUtils.repeat(" ", margin));
			}
			str.append(System.lineSeparator());
		}

		if (showGridLine) {
			for (int col = 0; col < table.getColumnCount(); col++) {
				str.append("+" + StringUtils.repeat("-", margin));
				str.append(StringUtils.repeat("-", numberOfCharOfEachCol[col]));
				str.append(StringUtils.repeat("-", margin));
			}
			str.append(System.lineSeparator());
		}
		return str.toString();
	}

	private int[] numberOfCharOfEachCol(JTable table) {
		int[] temp = new int[table.getColumnCount()];
		for (int col = 0; col < table.getColumnCount(); col++) {
			int numberOfchar = table.getColumnName(col).length();
			for (int row = 0; row < table.getRowCount(); row++) {
				if (table.getValueAt(row, col).toString().length() > numberOfchar) {
					numberOfchar = table.getValueAt(row, col).toString().length();
				}
			}
			temp[col] = numberOfchar;
		}
		return temp;
	}

}
