# Print formatted text table to console

# Example

See https://gitlab.com/quantr/java-text-table/tree/master/src/test/java

```
MyTableModel model = new MyTableModel();
model.data.add(new String[]{"Peter", "39", "Hong Kong", "789.432"});
model.data.add(new String[]{"Joanna", "19", "China", "12.432"});
model.data.add(new String[]{"Vincent", "29", "Hong Kong", "123213.432"});
model.data.add(new String[]{"Kenneth", "39", "Japan", "234234.432"});
model.data.add(new String[]{"Martin", "49", "Korean", "4563.432"});
MyTableRenderer renderer = new MyTableRenderer();
TextTable table = new TextTable(model, renderer);
table.eraseScreen = true;
table.showGridLine = true;
table.margin = 2;
table.sortColumn = 0;
table.ascending = false;
System.out.println(table.render());
```
		
![](https://www.quantr.hk/wp-content/uploads/2019/01/java-text-table.png)

# Author

Peter Cheung <peter@quantr.hk>
